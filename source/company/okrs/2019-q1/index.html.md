---
layout: markdown_page
title: "2019 Q1 OKRs"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### CEO: Grow Incremental ACV. More leads. Successful customers (standard implementation path, customer success based on their goals, DevOps maturity), Scalable marketing (Pipe-to-spend for all, double down on what works, more experiments with things like demo's, call us, and ebooks)

* Alliances: Measure the value partners can bring to the business.  3 references for each major public/private cloud, establish customer ability to purchase through marketplaces, joint marketing with key partners - 200+ leads per event
* CRO: Generate IACV from sales to higher tiers. X# new opportunities created from core accounts, Implement sales motion to show 100% of starter renewals the benefit and value of premium and ultimate, Identify two new already deployed large enterprise core customers in each SAL territory
* CRO: Faster delivery of customer value. Launch standard implementation model in enterprise segment, Measure time to completed implementation for all new enterprise accounts, X% of Q418 new accounts complete standard implementation phase 1 (Definition TBD)
* CMO: Recharge Inbound Marketing Engine. Top 10 Gitlab.com Pages re-architected for inbound, Inbound trial and contact-us strategy documented and baselined, Pages optimized for top 20 non-branded SEO target terms, Inbound strategy for Dev, Sec & Ops personas launched
* CMO: Achieve marketing process and visibility excellence. 100% visibility into any and all leads at any time, Full-funnel marketing dashboard published in Looker, Unified marketing calendar published.
* CMO: Achieve $18m in IACV pipeline (3x target) at beginning of Q2 FY19. 800 SAOs delivered to sales (SAOs as currently defined), X# of contact sales MQLS, Y# of EE trials.

### CEO: Popular next generation product. Triple secure value (3 stages, multiple teams). Grown use of stages (SMAU). iPhone app on iPad.

* Product: Increase product breadth. 25% (6 of 22) "new in 2019" categories at `minimal` maturity, CI/CD for iOS and Android development on GitLab.com, develop a Rails app on GitLab.com web IDE on iPad.
* Product: Increase product depth for existing categories. Consumption pricing for Linux, Windows, macOS for GitLab.com and self-managed, 50% (11 of 22) "new in 2018" categories at `viable` [maturity](/handbook/product/categories/maturity/). 
* Product: Grow use of GitLab for all stages of the DevOps lifecycle. Increase Stage Monthly Active Users ([SMAU](/handbook/product/growth/#smau)) 10% m/m for each stage, 3 reference customers using all stages concurrently.
* Alliances: evaluate qualifying for security competency with major clouds. Joint whitepaper, 1 security reference per major partner
* Alliances: evaluate if key partner services (AKS,EKS,GKE, VKE, etc) can grow use of stages.  Conversion of SCM customers to CI/CD through joint GTM.
* CRO: Increase CI usage. 5 of top 10 accounts not using CI yet from each region/segment using CI in production by end of Q1.
* Engineering
  * Development: Increase throughput 20%
    * Ops Backend: Increase BE teams's throughput by 20% and drive consistency week to week
    * Ops Backend: Define and implement an additional chart to complement each group's throughput chart
    * Dev Frontend + Backend: GA of GraphQL to increase througput for BE and FE. Resolve general topics like performance, abuse prevention and enjoyable development experience. Endpoints for Create and Plan relevant models. Implement new frontend features in Create and Plan only with GraphQL.
    * Frontend: Increase throughput and reduce implementation time of Frontend features by 20%. By having at least 50 gitlab-ui components at the end of Q1. Increase velocity of CSS adaptions. Improve tooling in GitLab itself for JS engineering (webpack map, visual diffing, etc.).
  * Frontend: Improve Performance of JS computation that is done on every page by 10%. Improve Initialisation execution by 20%. Reduce main bundle size by 20%.
  * Infrastructure: Run charts in production
  * Infrastructure: Get patroni in GitLab
  * Infrastructure: Automate error budgets
  * Quality: Get Insights in GitLab itself
  * Quality: Measure memory usage in GitLab
  * Quality: Productivity metrics: Rollup dashboard, review time, MR size
  * Security: Red Team with H1
  * Support: Reference architecture for 10k GitLab install
  * Support: Contribute to docs
  * UX: Improve the onboarding experience for new GitLab users. Increase week 1 retention by X%, Create onboarding journeys for each of our personas.
  * UX: Drive adoption of unknown/unused features. Create user journeys/flows for each stage group and determine X ways to introduce connections between them, Design a framework for onboarding users to unused features that increases adoption by 15% and can be reused across the application.
  * UX: Make GitLab usable by everyone. Introduce accessibility testing to gitlab-ui/csslab, Fix X accessibility issues outlined in our VPAT.


### CEO: Great team. Employed brand (known for all remote, great communication of total compensation, 10 videos per manager and up), Effective hiring (Faster apply to hire), ELO score per interviewer), Decision making effectiveness (kpis from original source and red/green, training for director group)
* CFO: Improve financial reporting and accounting processes to support growth and increased transparency.
    * Manager of Data Team: 100% of executive dashboards completed with goals and definitions 
    * Manager of Data Team: Public release of finance metric(s).
    * Director of Business Operations: Data Integrity Process (DIP) completed for ARR, Net and Gross Retention and Customer counts.
    * FinOps Lead: Integrated financial model that covers 100% of expense categories driven by IACV (as first iteration) and marketing funnel (as second iteration).
    * FinOps Lead: Release and change control process for financial reporting documented and added to handbook.
    * Sr Dir. of Legal: Detailed SoX compliance plan published internally and reviewed by Board. 
    * Sr Acctg Manager:  Key internal controls documented in handbook.
* CFO: Create scalable infrastructure for achieving headcount growth
    * Sr Dir. of Legal: 90% of team members covered by scalable employment solution
    * Payroll and Payments Lead: Automatation of contractor payroll completed.
* CFO: Improve company wide operational processes
    * Director of Bus Ops: Roadmaps for all staffed business operations functions are shipped and Q2 iteration by EOQ
    * Controller: Zuora upgrade to orders which will allow for ramped deals, multiple amendments on single quote and MRR by subscription reporting.

* Engineering: Develop comp roadmap for every engineering role
    * Ops Backend: Define first iteration for BE role framework (Software Engineer, Senior Software Engineer, Staff Software Engineer)
* Engineering: Unify vacancy descriptions and exp content for all engineering roles
  * Development: Hire to plan
  * Development: Get every engineer in one customer conversation

* CCO: Improve the candidate experience at GitLab. This will require all members of the interview team to prioritize interviewing and inputting interview feedback quickly, in addition to process improvements and proper staffing of the recruiting team and wll be measured by a reduction to the time a candidate is in process to 30 days from application to Offer and 80% for all interviewers inputting their feedback within 24 hours.

* CCO:  Improve GitLab's training and development progress, by hiring an L&D specialist (by the end of Q1), increasing the tuition remimbursement utilization by 10%, and defining metrics for each training that inform on the value of the training (80% of attendees rate the training as valuable and/or actionable, key business improvement metrics that should be impacted by the training, follow-up survey 30-60 after training to guage applicablility). The improvement should also be reflected in the annual Engagement survey. 

* CCO:  Improve GitLab's Employer brand, starting with hiring a Employer Branding specialist, identifying 5-10 locations to focus our branding and sourcing (25% increase in pipeline from these locations); collaborating with Marketing to ensure that recruiting activities are integrated into at least 95% for conferences, and increasing our responsiveness to Social Media posts (respond to 80% for Glassdoor posts)
  

