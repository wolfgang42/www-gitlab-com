---
layout: markdown_page
title: "Why Premium?"
---

## GitLab Premium

| Premium helps IT organizations scale their DevOps delivery supporting high availability, geographic replication, progressive deployment, advanced configuration, and consistent standards. | ![Canary Deployment](https://docs.gitlab.com/ee/user/project/img/deploy_boards_canary_deployments.png) |

### **Developer Productivity**

| Ensure your developers are always able to be productive, regardless of where they are geographically located.  | ![GitLab Geo](https://docs.gitlab.com/ee/administration/geo/replication/img/geo_overview.png){: .margin-right20 .margin-left20 .image-width70pct }   |

| **Productivity** |  **Value** |
| High Availability | Configure GitLab to minimize downtime and outages, ensuring developers are able to work at all times.  |
| Geographic |  Enable local replicas of the GitLab data, to reduce latency and increase developer productivity.  |

### **Streamline Project Planning**

|  Manage multiple projects (programs) with intuitive and easy to use dashboards and reports to track issues and milestones across multiple projects.  |  ![Assignee Lists](https://docs.gitlab.com/ee/user/project/img/issue_board_assignee_lists.png)  |

| Planning    | Value |
| --------- | ------------ |
| Backlog management | Simplify tracking, scoping and planning future work with group level backlog management on multiple issue boards.   |
| Milestone Boards/Lists | Use milestones to visualize the future work to be delivered in future releases/milestones.   |
| Assignee Boards/Lists |  Streamline assignment of work to team members in a graphical assignment board.   |
| Group Issue Boards |  Visually manage programs (groups) with multiple issue boards where work can be dynamically assigned and tracked.   |

### **Deploy with confidence**

| Accelerate software delivery with integrated deployment and release management. | ![Multiple Project Pipeline Graphs](https://docs.gitlab.com/ee/ci/img/multi_project_pipeline_graph.png) |

| Deploy    | Value |
| --------- | ------------ |
| Feature Flags | Rollout code changes and then dynamically enable or disable specific features. |
| Deploy Boards | Visualize and plan deployments to each environment |
| Incremental Deployments | Sequential rollout of code changes minimizes risk |
| Canary Deployments | Limit risks and roll out changes to a minimal set of end users before initiating rollout to the entire population. |
| Multi Project Pipelines | Link CI pipelines from multiple projects to deliver integrated solutions |
| Maven Repository | Maintain library of binary versions of different builds. |
| Protected Environments | Establish controls and limit the access to change specific environments |

### **Manage the Development Process**  

| Simplify compliance with and traceability with enterprise features built into the developer's workflow. | ![Merge Request Reviews](https://about.gitlab.com/images/feature_page/screenshots/batch-comments.png) |

| Manage Development     | Value |
| --------- | ------------ |
| Verified Committer | Ensure only authorized and verified team members are allowed to commit to the project   |
| Require Signed Commits | Enforce policy to require signed commits from contributors  |
| Merge Request Reviews | Track and manage code reviews and feedback with built in merge request reviews |
| Group and File Templates | Establish consistent and standard practices |
