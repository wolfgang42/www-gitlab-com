---
layout: job_family_page
title: "Manager, Data and Analytics"
---

## Responsibilities

* Ensure the Company’s cloud and on-premise data is centralized into a single data warehouse that can support data analysis requirements from all functional groups of the Company. See our bizops effort, [Meltano](https://gitlab.com/meltano/meltano/).
* Be the data expert supporting cross-functional teams, gather data from various sources, and build automated reporting to democratize data across the company.
* Hold regular 1:1’s with all members of the Data and Analytics Team.
* Manage development priorities of analytics dashboards and data pipelines.
* Represent the Data and Analytics Team in different company functions - be an advocate for holistic dataflow systems thinking.
* Create and execute a plan to develop and mature our ability to measure and optimize usage growth and our user journey.
* Provide analyses to identify opportunities and explain trends
* Implement a set of processes that ensure any changes in transactional system architecture are documented and their impact on the company’s overall data integrity are considered prior to changes being made.
* Collaborate with all functions of the company to ensure data needs are addressed.
* Create a common data framework so that all company data can be analyzed in a unified manner.
* This position reports to the CFO.


## Requirements

* 2+ years hands on experience in a data analytics/engineering/science role
* 2+ years managing a team of 2 or more data analysts/engineers/scientists
* Demonstrably deep understanding of SQL and relational databases (Snowflake preferred)
* Ability to reason holistically about end-to-end data systems: from ETL to Analysis to Reporting
* Hands on experience working with with Python
* Experience building and maintaining data pipelines (Airflow, Luigi, GitLab CI)
* Experience building reports and dashboards in a data visualization tool like Tableau, Birst, or Looker
* Experience with open source data warehouse tools
* Be passionate about data, analytics, and automation, especially in applying software engineering principles to data science and analytics
* Experience working with large quantities of raw, disorganized data
* Experience with Salesforce, Zuora, Zendesk and Marketo
* Strong written and verbal communication skills
* Share and work in accordance with our values
* Must be able to work in alignment with Americas timezones
* Successful completion of a [background check](/handbook/people-operations/code-of-conduct/#background-checks)

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

* Selected candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a first interview with our CFO
* Next, candidates will be invited to schedule a second interview with our Finance Operations and Planning Lead
* Next, candidates will be invited to schedule a third interview with members of the BizOps team
* Finally, candidates may be asked to interview with our CEO

Additional details about our process can be found on our [hiring page](handbook/hiring).
