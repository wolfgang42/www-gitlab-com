---
layout: markdown_page
title: "Reddit response workflow"
---

## Overview

Monitoring of all mentions of gitlab within Reddit. There is currently no way to filter whether the mention of GitLab is only from a URL ([example here](https://www.reddit.com/r/promotereddit/comments/a7eeb6/rtheprivacymachine_privacy_and_security/)), so there may potentially be a high volume of noise.

## Workflow

1. Go through each ticket in Zendesk in the `Reddit` view

2. See if the comment or post has received a response

3. Respond if necessary using your **personal** Reddit account
   * If needed, ask an expert for help
   * Mark post as `On Hold` if you're waiting for an expert to reply
   * If your response asks a question or sparks conversation, mark the ticket as `Pending`
4. If comment/post does not need a response but is still good or valuable, upvote it and apply `Reddit > Upvote Only` macro
   * If it is particularly insightful or useful, it may be a good idea to share it on an appropriate Slack channel for visibility
5. Mark the ticket as `Solved`

## Best practices

* Always be kind and understanding, no matter how the other person acts
* Use a **personal** reddit account, not a company one. It makes comments seem much more authentic
* If you use Reddit a lot, it may be useful to create a separate, more professional (but still personal) account
* Upvote good posts/comments
* Only downvote if a post is abusive or spreads false information. Don't just downvote because someone doesn't like the product or mentions a competitor
* [Brush up on your "reddiquette"](https://www.reddit.com/wiki/reddiquette)
* [Make sure your comments are formatted well](https://www.reddit.com/wiki/commenting)

## Automation

All mentions of GitLab, whether comments or new posts, are handled by Zapier. It first creates a ticket in Zendesk under the `Reddit` view, and then posts to the `#reddit` Slack channel. In Slack, new comments are posted by a bot with a blue icon, and new posts with an orange icon.

There are often bot posts that include GitLab URLs that generate a lot of unwanted noise. There is a step in the Zapier automation that blacklists bots that do this. If you notice a large number of posts coming from a bot, please add it to the blacklist by following these steps in Zapier:
1. In Zapier, go to `Zaps` > `Community Advocacy` > `New Reddit Mention`
2. Go to the `Filter Known Bots` step
3. Select `Filter Setup & Testing`
4. Click `+ OR`
5. For the first dropdown, select `Username`
6. For the second, select `(Text) Does not exactly match`
7. In the third box, paste the name of the bot to exclude
8. Run a test and continue
9. Zapier automatically turns off processes that have been modified, so make sure you turn it back on